import React from "react";
import { MemoryRouter, Switch, Route } from "react-router-dom";
import Jumbotron from "react-bootstrap/Jumbotron";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import { LinkContainer } from "react-router-bootstrap";

import Invoice from "./screens/Invoice.js";
import Invoices from "./screens/Invoices.js";

import "./App.css";

const App = () => (
  <MemoryRouter>
    <Container className="p-3">
      <Jumbotron>
        <h1 className="header">Accounting app</h1>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/" id="home_btn">Accountingn</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <LinkContainer to="/" id="invoices_btn">
                <Nav.Link href="/">Invoices</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/create-invoice" id="create_invoice_btn">
                <Nav.Link href="/create-invoice">Create Invoice</Nav.Link>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <Switch>
          <Route path="/create-invoice">
            <Invoice></Invoice>
          </Route>
          <Route path="/users"></Route>
          <Route path="/">
            <Invoices />
          </Route>
        </Switch>
      </Jumbotron>
    </Container>
  </MemoryRouter>
);

export default App;
