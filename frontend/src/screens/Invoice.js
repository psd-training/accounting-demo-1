import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import React, { useState } from "react";
import axios from "axios";

const Invoice = () => {
  const [items, setItems] = useState([]);
  const [itemTitle, setItemTitle] = useState("");
  const [itemQty, setItemQty] = useState(1);
  const [itemUnitPrice, setItemUnitPrice] = useState(0.0);
  const [ico, setIco] = useState("");
  const [dueDate, setDueDate] = useState();
  const [invoiceNumber, setInvoiceNumber] = useState("");

  const handleSubmit = () => {
    axios.post("/api/invoices", {
      ico: ico,
      invoiceNumber: invoiceNumber,
      dueDate: dueDate,
      items: items,
    });
  };

  const addItem = () => {
    setItems((items) => [
      ...items,
      {
        title: itemTitle,
        quantity: itemQty,
        unitPrice: itemUnitPrice,
        amount: itemUnitPrice * itemQty,
      },
    ]);
    setItemTitle("");
    setItemQty(1);
    setItemUnitPrice(0.0);
  };

  return (
    <>
      <Form>
        <Form.Group controlId="formNumber">
          <Form.Label>Invoice #</Form.Label>
          <Form.Control
            type="text"
            placeholder="#"
            value={invoiceNumber}
            onChange={(e) => setInvoiceNumber(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formIco">
          <Form.Label>ICO</Form.Label>
          <Form.Control
            type="number"
            placeholder="ICO"
            value={ico}
            onChange={(e) => setIco(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formDueDate">
          <Form.Label>Due Date</Form.Label>
          <Form.Control
            type="date"
            value={dueDate}
            onChange={(e) => setDueDate(e.target.value)}
          />
        </Form.Group>

        <Form.Row>
          <Form.Group as={Col} sm="1" controlId="formItemQty">
            <Form.Label>Qty</Form.Label>
            <Form.Control
              type="number"
              name="itemQty"
              value={itemQty}
              onChange={(e) => setItemQty(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formItemTitle">
            <Form.Label>Item</Form.Label>
            <Form.Control
              type="text"
              name="itemTitle"
              value={itemTitle}
              onChange={(e) => setItemTitle(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col} sm="2" controlId="formItemUnitPrice">
            <Form.Label>Unit Price</Form.Label>
            <Form.Control
              type="number"
              name="itemUnitPrice"
              step="0.01"
              value={itemUnitPrice}
              onChange={(e) => setItemUnitPrice(e.target.value)}
            />
          </Form.Group>
          <Col
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
            }}
            sm="1"
          >
            <Button onClick={addItem} variant="primary" id="addItemBtn">
              Add
            </Button>
          </Col>
        </Form.Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Qty</th>
              <th>Title</th>
              <th>Unit Price</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {items.map((value, index) => {
              return (
                <tr name="item" data-item={value.title}>
                  <td name="qty_col">{value.quantity}</td>
                  <td name="title_col">{value.title}</td>
                  <td name="price_col">{Number(value.unitPrice).toFixed(2)}</td>
                  <td name="total_col">{Number(value.amount).toFixed(2)}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>

        <Button variant="primary" type="submit" onClick={handleSubmit}>
          Create
        </Button>
      </Form>
    </>
  );
};

export default Invoice;
