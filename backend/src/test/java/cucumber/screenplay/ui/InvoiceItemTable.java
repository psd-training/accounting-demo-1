package cucumber.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class InvoiceItemTable {
    private static final By TITLE = By.name("title_col");
    public static final Target ITEM_TITLE = Target.the("item title").located(TITLE);

    public static By totalFor(String item) {
        return By.xpath("//tr[@data-item=\"" + item + "\"]/td[@name=\"total_col\"]");
    }
}
