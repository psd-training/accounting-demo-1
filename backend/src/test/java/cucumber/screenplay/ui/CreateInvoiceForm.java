package cucumber.screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class CreateInvoiceForm {
    public static final Target INVOICE_NUMBER_FIELD = Target.the("Invoice number field").locatedBy("#formNumber");
    public static final Target ICO_FIELD = Target.the("ICO field").locatedBy("#formIco");
    public static final Target DUE_DATE_FIELD = Target.the("Due Date field").locatedBy("#formDueDate");
    public static final Target ITEM_QTY_FIELD = Target.the("Item qty field").locatedBy("#formItemQty");
    public static final Target ITEM_TITLE_FIELD = Target.the("Item title field").locatedBy("#formItemTitle");
    public static final Target ITEM_PRICE_FIELD = Target.the("Item price field").locatedBy("#formItemUnitPrice");
    public static final Target ITEM_ADD_BUTTON = Target.the("Add Item button").locatedBy("#addItemBtn");

    public static Target getField(String name) {
        switch (name) {
        case "Quantity": return ITEM_QTY_FIELD;
        case "Description": return ITEM_TITLE_FIELD;
        case "Price": return ITEM_PRICE_FIELD;
        default: throw new IllegalArgumentException("Unknow field: " + name);
        }
    }
}
