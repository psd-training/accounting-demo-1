package cucumber.screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8080")
public class AccountingApp extends PageObject {
    public static final Target HOME_BUTTON = Target.the("home button").locatedBy("#home_btn");
    public static final Target CREATE_INVOICE_BUTTON = Target.the("create invoice button").locatedBy("#create_invoice_btn");
}
