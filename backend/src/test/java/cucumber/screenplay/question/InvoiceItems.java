package cucumber.screenplay.question;

import java.util.List;

import org.openqa.selenium.By;

import cucumber.screenplay.ui.InvoiceItemTable;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class InvoiceItems {

    public static Question<List<String>> displayed() {
        return actor -> Text
                .of(InvoiceItemTable.ITEM_TITLE)
                .viewedBy(actor)
                .asList();
    }

    public static Question<Double> totalOf(String item) {
        return actor -> Text
                .of(InvoiceItemTable.totalFor(item))
                .viewedBy(actor)
                .asDouble();
    }

}
