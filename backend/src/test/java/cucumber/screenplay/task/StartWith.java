package cucumber.screenplay.task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import cucumber.screenplay.ui.AccountingApp;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;

public class StartWith implements Task {
    private AccountingApp app;
    private Target initialPage;

    public StartWith(Target initialPage) {
        this.initialPage = initialPage;
    }

    @Override
    @Step("{0} starts with app")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(app), Click.on(initialPage)
        );
    }

    public static StartWith homePage() {
        return instrumented(StartWith.class, AccountingApp.HOME_BUTTON);
    }

    public static StartWith createInvoiceForm() {
        return instrumented(StartWith.class, AccountingApp.CREATE_INVOICE_BUTTON);
    }

}
