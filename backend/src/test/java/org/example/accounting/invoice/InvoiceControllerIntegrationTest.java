package org.example.accounting.invoice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class InvoiceControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private InvoiceRepository repository;

    @Test void testCreate() throws Exception {
        Invoice invoice = Invoice.builder()
                .invoiceNumber("001")
                .dueDate(new Date())
                .ico(123)
                .items(Arrays.asList(Item.builder()
                        .quantity(1)
                        .unitPrice(1.0)
                        .description("item")
                        .build()))
                .build();
        invoice.setItems(Arrays.asList(Item.builder().description("item").quantity(1).unitPrice(1.0).build()));
        String json = objectMapper.writeValueAsString(invoice);
        System.out.println(json);
        MvcResult result = mockMvc.perform(post("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated())
                .andReturn();

        Invoice response = objectMapper.readValue(result.getResponse().getContentAsString(), Invoice.class);
        Optional<Invoice> savedInvoiceOpt = repository.findById(response.getId());

        assertTrue(savedInvoiceOpt.isPresent());
        Invoice savedInvoice = savedInvoiceOpt.get();
        assertEquals(invoice.getInvoiceNumber(), savedInvoice.getInvoiceNumber());
        assertEquals(invoice.getIco(), savedInvoice.getIco());
        assertEquals(invoice.getItems(), savedInvoice.getItems());
        assertEquals(invoice.calculateTotal(), savedInvoice.getTotal());
        assertNotNull(savedInvoice.getCreated());

    }
}
