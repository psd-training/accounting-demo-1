package org.example.accounting.invoice;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = InvoiceController.class)
public class InvoiceControllerWebTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private InvoiceRepository repository;

    @Test void testCreate() throws Exception {
        Mockito.when(repository.save(any(Invoice.class))).then(invocation -> {
            Invoice argument = (Invoice) invocation.getArguments()[0];
            argument.setId("generated");
            return argument;
        });
        Invoice invoice = Invoice.builder()
                .invoiceNumber("001")
                .dueDate(new Date())
                .ico(123)
                .items(Arrays.asList(Item.builder()
                        .quantity(1)
                        .unitPrice(1.0)
                        .description("item")
                        .build()))
                .build();
        invoice.setItems(Arrays.asList(Item.builder().description("item").quantity(1).unitPrice(1.0).build()));
        MvcResult result = mockMvc.perform(post("/api/invoices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invoice)))
                .andExpect(status().isCreated())
                .andReturn();
        String responseBody = result.getResponse().getContentAsString();
        invoice.setId("generated");

        JSONAssert.assertEquals(objectMapper.writeValueAsString(invoice), responseBody, false);
        Invoice createdInvoice = objectMapper.readValue(responseBody, Invoice.class);
        Assertions.assertNotNull(createdInvoice.getCreated());
    }
}
