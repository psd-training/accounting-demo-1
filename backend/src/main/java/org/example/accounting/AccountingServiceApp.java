package org.example.accounting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class AccountingServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(AccountingServiceApp.class, args);
    }
}